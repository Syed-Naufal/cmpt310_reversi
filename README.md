# Reversi Using Monte Carlo Tree Search 
## Description:
A program with two instances of Monte Carlo Tree Search (MCTS) that play against each other. One version is using pure MCTS and the other makes use of MCTS in addition to heuristics to make better, more informed decisions. The goal being to beat the pure MCTS on a consistent basis.  

Completed March 2020 for CMPT 310.