#ifndef VECTORWRAPPER_H
#define VECTORWRAPPER_H
#define VECLEN 30

#include <vector>
using namespace std;

template <typename T>
class VectorWrapper {
public:
	VectorWrapper();
    VectorWrapper(vector<T> v, int s);
    VectorWrapper(int s);
    VectorWrapper(const VectorWrapper &v);
    void resize_wrapper(int s);
	T get_vec_data(int i);
    vector<T> vec{};
    int size;
};

template <typename T>
VectorWrapper<T>::VectorWrapper() {
	vec = vector<T>(VECLEN);
	size = 0;
}

template <typename T>
VectorWrapper<T>::VectorWrapper(vector<T> v, int s) {
    vec = v;
    size = s;
}

template <typename T>
VectorWrapper<T>::VectorWrapper(int s): vec(s), size(0){}

template <typename T>
VectorWrapper<T>::VectorWrapper(const VectorWrapper &v) {
    vec = v.vec;
    size = v.size;
}

template <typename T>
T VectorWrapper<T>::get_vec_data(int i) {
	return vec[i];
}

template <typename T>
void VectorWrapper<T>::resize_wrapper(int s) {
    // should be called if the vec was previously initialized
    vec.resize(s);
    size = s;
}


#endif //VECTORWRAPPER_H
