#ifndef BOARD_H
#define BOARD_H

#include "VectorWrapper.h"

#define BoardSize 8
#define MAXINDEX 7
typedef int (*DoublePointer)[BoardSize];

class Index {
public:
	Index(int x_in, int y_in);
	Index();
	int x;
	int y;
};

class Board {
private:
    int currentBoardArray[BoardSize][BoardSize] = {0};

	bool check_adjacent_squares(int x, int y, int player, int enemy); 
	bool check_right_row(int xbase, int ybase, int player); 
	bool check_left_row(int xbase, int ybase, int player);
	bool check_top_col(int xbase, int ybase, int player);
	bool check_bottom_col(int xbase, int ybase, int player);
	bool check_bottom_right_diag(int xbase, int ybase, int player);
	bool check_top_left_diag(int xbase, int ybase, int player);
	bool check_bottom_left_diag(int xbase, int ybase, int player);
	bool check_top_right_diag(int xbase, int ybase, int player);

public:
    Board();
    Board(int array[BoardSize][BoardSize]);
    Board(const Board &board);
    int check_win();
    bool check_draw();
	void update_board(int[BoardSize][BoardSize]);
    void display();
    int count_characters(int character);
    VectorWrapper<Index> get_legal_positions(int player);
    DoublePointer get_board_array();
};

#endif //BOARD_H
